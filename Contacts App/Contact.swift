//
//  Data.swift
//  Contacts App
//
//  Created by Михаил on 24.09.2021.
//

import Foundation
import RealmSwift



class Contact : Object {
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var dateOfBirth: String = ""
    @objc dynamic var companyName: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var phoneNumber: String = ""
}
