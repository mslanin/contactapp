//
//  ViewController.swift
//  Contacts App
//
//  Created by Михаил on 23.09.2021.
//
import UIKit
import RealmSwift

class ContactListViewController: UITableViewController {
    @IBOutlet private weak var addButtonOutlet: UIBarButtonItem!
    @IBOutlet private weak var searchBar: UISearchBar!
    
    private var contacts: Results<Contact>?
    
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTable()
        loadContacts()
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    private func setupTable() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    // MARK: - Data methods
    private func loadContacts() {
        contacts = realm.objects(Contact.self).sorted(byKeyPath: "firstName", ascending: true)
        tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
        
        guard let contact = contacts?[indexPath.row] else {
            return cell
        }
        
        if contact.firstName.isEmpty,
           contact.lastName.isEmpty {
            cell.textLabel?.text = contact.phoneNumber.isEmpty ? contact.email : contact.phoneNumber
        } else {
            cell.textLabel?.text = contact.firstName + " " + contact.lastName
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let contact = contacts?[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ContactData") as! ContactDataViewController
        
        vc.contact = contact
        
        present(vc, animated: true, completion: nil)
    }

    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ContactData") as! ContactDataViewController
        
        present(vc, animated: true, completion: nil)
    }
}

// MARK - Search Bar Delegate
extension ContactListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let firstNamePredicate = NSPredicate(format: "firstName CONTAINS[cd] %@", searchBar.text ?? "")
        let lastNamePredicate = NSPredicate(format: "lastName CONTAINS[cd] %@", searchBar.text ?? "")
        let emailPredicate = NSPredicate(format: "email CONTAINS[cd] %@", searchBar.text ?? "")
        let phoneNumberPredicate = NSPredicate(format: "phoneNumber CONTAINS[cd] %@", searchBar.text ?? "")
        let query = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or,
                                        subpredicates: [firstNamePredicate,
                                                        lastNamePredicate,
                                                        emailPredicate,
                                                        phoneNumberPredicate])
        
        contacts = contacts?.filter(query).sorted(byKeyPath: "firstName", ascending: true)
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadContacts()
            searchBar.resignFirstResponder()
        }
    }
}
