//
//  ContactDataViewController.swift
//  Contacts App
//
//  Created by Михаил on 23.09.2021.
//

import UIKit
import RealmSwift

class ContactDataViewController: UIViewController {
    @IBOutlet private weak var firstNameField: UITextField!
    @IBOutlet private weak var lastNameField: UITextField!
    @IBOutlet private weak var dateOfBirthField: UITextField!
    @IBOutlet private weak var companyNameField: UITextField!
    @IBOutlet private weak var emailField: UITextField!
    @IBOutlet private weak var phoneNumberField: UITextField!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton!
    
    let realm = try! Realm()
    var contact: Contact?
    
    override func viewDidLoad () {
        if contact == nil {
            toggleEditMode(on: true)
        } else {
            toggleEditMode(on: false)
        }
        
        setup()
    }
    
    private func setup () {
        firstNameField.text = contact?.firstName
        lastNameField.text = contact?.lastName
        companyNameField.text = contact?.companyName
        dateOfBirthField.text = contact?.dateOfBirth
        emailField.text = contact?.email
        phoneNumberField.text = contact?.phoneNumber
        
        phoneNumberField.delegate = self
        firstNameField.delegate = self
        lastNameField.delegate = self
    }
    
    private func toggleEditMode(on: Bool) {
        firstNameField.isEnabled = on
        lastNameField.isEnabled = on
        dateOfBirthField.isEnabled = on
        companyNameField.isEnabled = on
        emailField.isEnabled = on
        phoneNumberField.isEnabled = on
        editButton.isHidden = on
        saveButton.isHidden = !on
        deleteButton.isHidden = !on
    }
    
    // MARK: - Data Methods
    private func saveContact(_ contact: Contact) {
        do {
            try! realm.write {
                realm.add(contact)
            }
        }
    }
    
    private func updateContact() {
        do {
            try! realm.write {
                contact?.firstName = firstNameField.text ?? ""
                contact?.lastName = lastNameField.text ?? ""
                contact?.dateOfBirth = dateOfBirthField.text ?? ""
                contact?.companyName = companyNameField.text ?? ""
                contact?.email = emailField.text ?? ""
                contact?.phoneNumber = phoneNumberField.text ?? ""
            }
        }
    }
    
    private func deleteContact() {
        guard let contact = contact else {
            return
        }
        
        do {
            try! realm.write {
                realm.delete(contact)
            }
        }
    }
    
    // MARK: - Format phone number
    private func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "+# (###) ###-##-##"
        var result = ""
        var index = cleanPhoneNumber.startIndex
        
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "#" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        
        return result
    }
    
    // MARK: - First Name/Last Name Validation
    private func isValidName(_ name: String) -> Bool {
        let nameRegex = "^[a-zA-Z-а-яА-Я-]+$"
        let namePredicate = NSPredicate(format:"SELF MATCHES %@", nameRegex)
        return namePredicate.evaluate(with: name)
    }
    
    // MARK: - Email Validation
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: email)
    }
    
    private func showAlert(message: String) {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Назад", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    // MARK: - Buttons
    @IBAction func editButtonAction(_ sender: UIButton) {
        toggleEditMode(on: true)
        firstNameField.becomeFirstResponder()
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        guard firstNameField.text != "" ||
              lastNameField.text != "" ||
              emailField.text != "" ||
              phoneNumberField.text != "" else {
                    showAlert(message: "Заполните хотя бы одно необходимое поле")
                    return
                }
        
        guard (isValidEmail(emailField.text ?? "") || emailField.text == "") else {
                  showAlert(message: "Проверьте правильность набранного Email")
                  return
              }
        
        if contact != nil {
            updateContact()
        } else {
            let newContact = Contact()
            newContact.firstName = firstNameField.text ?? ""
            newContact.lastName = lastNameField.text ?? ""
            newContact.dateOfBirth = dateOfBirthField.text ?? ""
            newContact.companyName = companyNameField.text ?? ""
            newContact.email = emailField.text ?? ""
            newContact.phoneNumber = phoneNumberField.text ?? ""
            
            self.saveContact(newContact)
            contact = newContact
        }
        
        self.toggleEditMode(on: false)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        deleteContact()
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Text Field Delegate
extension ContactDataViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return false
        }
        
        var newString = (text as NSString).replacingCharacters(in: range, with: string)
        
        switch textField {
        case firstNameField, lastNameField:
            if (isValidName(newString) || newString.isEmpty) {
                return true
            } else {
                return false
            }
        case phoneNumberField:
            let inputPrefix = "+7"
            
            if newString.range(of: inputPrefix)?.lowerBound != newString.startIndex {
                let prefixContainsText = inputPrefix.range(of: newString)?.lowerBound == inputPrefix.startIndex
                newString = !prefixContainsText ? inputPrefix + newString : inputPrefix
            }
            textField.text = formattedNumber(number: newString)
            
            return false
        default:
            return true
        }
    }
}
